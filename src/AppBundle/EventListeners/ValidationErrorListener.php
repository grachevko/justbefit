<?php

namespace AppBundle\EventListeners;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * @author Konstantin Grachev <me@grachevko.ru>
 */
final class ValidationErrorListener implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => 'onKernelView',
        ];
    }

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        $errors = $event->getControllerResult();

        if (!$errors instanceof ConstraintViolationList) {
            return;
        }

        $messages = [];
        foreach ($errors as $error) {
            $messages[$error->getPropertyPath()] = $error->getMessage();
        }

        $event->setResponse(new JsonResponse(['errors' => $messages], Response::HTTP_BAD_REQUEST));
    }
}
