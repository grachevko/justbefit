<?php

namespace AppBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}.
 */
class AppExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $dir = __DIR__.'/../Resources/config';
        $loader = new Loader\YamlFileLoader($container, new FileLocator($dir));

        $finder = new Finder();
        $finder->files()->name('*.yml')->depth('== 0')->in($dir);
        foreach ($finder as $file) {
            $loader->load($file->getBasename());
        }
    }
}
