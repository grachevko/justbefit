<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        self::assertEquals(200, $client->getResponse()->getStatusCode());
        self::assertContains('Welcome to Symfony', $crawler->filter('#container h1')->text());
    }

    public function testNew()
    {
        $client = static::createClient();

        $client->request('GET', '/new');
        self::assertEquals(405, $client->getResponse()->getStatusCode());

        $client->request('POST', '/new');
        self::assertEquals(400, $client->getResponse()->getStatusCode());

        $container = $client->getContainer();
        $userRepository = $container->get('doctrine.orm.default_entity_manager')->getRepository(User::class);

        $username = 'vasya';
        $groupname = 'students';

        self::assertNull($userRepository->findOneBy(['name' => $username]));

        $client->request(
            'POST',
            '/new',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(['username' => $username, 'groupname' => $groupname])
        );

        /** @var User $user */
        $user = $userRepository->findOneBy(['name' => $username]);
        self::assertNotNull($user);
        self::assertTrue($user->hasGroups($groupname));

        $groupname2 = 'developer';
        self::assertFalse($user->hasGroups($groupname2));

        $client->request(
            'POST',
            '/new',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(['username' => $username, 'groupname' => $groupname2])
        );

        self::assertTrue($user->hasGroups($groupname2));
    }
}
