<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Request\NewRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/new", name="new_route")
     * @Method("POST")
     */
    public function newAction(Request $request)
    {
        if (0 !== strpos($request->headers->get('Content-Type'), 'application/json')) {
            throw new BadRequestHttpException();
        }

        $model = new NewRequest((array) json_decode($request->getContent(), true));

        if (count($errors = $this->get('validator')->validate($model))) {
            return $errors;
        }

        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository(User::class)->findOneBy(['name' => $model->username]) ?? new User($model->username);

        $user->addGroup($model->groupname);

        $em->persist($user);
        $em->flush();

        return new JsonResponse();
    }
}
