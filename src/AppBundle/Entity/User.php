<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity()
 */
class User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var ArrayCollection|Group[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Group", mappedBy="user", cascade={"persist", "remove"})
     */
    private $groups;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->groups = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function hasGroups(string $name)
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('name', $name));

        return (bool) $this->groups->matching($criteria)->count();
    }

    /**
     * @param $name
     */
    public function addGroup(string $name)
    {
        if (!$this->hasGroups($name)) {
            $this->groups->add(new Group($name, $this));
        }
    }
}

